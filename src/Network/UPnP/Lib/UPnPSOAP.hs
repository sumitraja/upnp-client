{-# LANGUAGE OverloadedStrings #-}

module Network.UPnP.Lib.UPnPSOAP (
    soapBody,
    soapEnvelope,
    SoapActionProvider(..),
    SOAPFault(..),
    parseSOAPError,
    SOAPErrorParser,
    SOAPBodyParser,
    createSoapRequest,
    parseSOAPEnvelope,
    SOAPEnvelope(..)
) where

import ClassyPrelude
import qualified Data.ByteString.Char8 as C
import Network.UPnP.Lib.RFC2616
import Text.XML as XML
import Text.XML.Cursor
import Text.Hamlet.XML
import qualified Data.Text as T
import Control.Applicative
import Control.Monad.Except
import qualified Data.Map as Map
import qualified Network.HTTP.Conduit as HTTP
import Data.CaseInsensitive
import Data.Maybe
import Network.UPnP.Lib.XMLStuff

class SoapActionProvider a where
  getSoapAction :: a -> T.Text

soapEnvelopeNS :: T.Text -> Name
soapEnvelopeNS = nameWithNS "http://schemas.xmlsoap.org/soap/envelope/"

soapEnvelope :: (ToXML body) => body -> XML.Element
soapEnvelope body =
  let
        name = soapEnvelopeNS "Envelope"
        encodingStyle = "{http://schemas.xmlsoap.org/soap/envelope/}encodingStyle" :: Name
        attrs = Map.singleton encodingStyle "http://schemas.xmlsoap.org/soap/encoding/"
  in
        Element { elementName = name, elementAttributes = attrs, elementNodes = [soapBody body]}

soapDocument :: (ToXML body) => body -> Document
soapDocument body = Document (Prologue [] Nothing []) root []
  where
    root = soapEnvelope body

soapBody :: (ToXML body) => body -> Node
soapBody body = NodeElement Element { elementName = name, elementAttributes = Map.empty, elementNodes = [makeXML body]}
    where name = soapEnvelopeNS "Body"

createSoapRequest :: (SoapActionProvider body, ToXML body) => HTTP.Request -> body -> HTTP.Request
createSoapRequest template command = template { HTTP.requestHeaders = headers, HTTP.requestBody = rbdy}
  where
    header = ("SOAPACTION", "\"" <> encodeUtf8 (getSoapAction command) <> "\"")
    headers = header : HTTP.requestHeaders template
    rbdy = HTTP.RequestBodyLBS (renderLBS def (soapDocument command))

data SOAPFault a = SOAPFault { faultcode :: T.Text, faultstring :: T.Text, detail :: a} deriving (Eq, Show)

data SOAPEnvelope a b = SOAPFaultEnvelope (SOAPFault a) | SOAPEnvelope b deriving (Eq, Show)

newtype SOAPParser a b = SOAPParser {
  runParser :: Either String (SOAPEnvelope a b)
}

type SOAPErrorParser a = XMLParser a
type SOAPBodyParser b = XMLParser b

parseSOAPEnvelope :: Cursor -> SOAPErrorParser a -> SOAPBodyParser b -> Either XMLParseError (SOAPEnvelope a b)
parseSOAPEnvelope cur errorParser bodyParser = do
  envNode <- chkName cur (soapEnvelopeNS "Envelope")
  bodyNode <- extractElementOrError elementMaker "Body" envNode
  let faultNode = extractElementOrError elementMaker "Fault" bodyNode
  case faultNode of
    Right x -> do
      sfault <- parseSOAPError x errorParser
      Right (SOAPFaultEnvelope sfault)
    Left nf -> case filter (filterer . node) (child bodyNode) of
        (x:xs) -> if null xs then
            do
              bp <- bodyParser x
              return (SOAPEnvelope bp)
          else Left ("SOAP body has more than one child element: " ++ tshow x)
        [] -> Left "SOAP body is empty"
  where
    elementMaker = element . soapEnvelopeNS
    filterer :: Node -> Bool
    filterer (NodeElement _) = True
    filterer _ = False

parseSOAPError :: Cursor -> SOAPErrorParser a -> Either XMLParseError (SOAPFault a)
parseSOAPError soapErrorsNode errorBodyParser = do
  faultCode <- extractContentOrError laxElement "faultcode" soapErrorsNode
  faultString <- extractContentOrError laxElement "faultstring" soapErrorsNode
  detail <- extractElementOrError laxElement "detail" soapErrorsNode
  errordetail <- errorBodyParser detail
  return (SOAPFault faultCode faultString errordetail)

{-- SOAP ERROR
<s:Envelope
 xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
 <s:Body><s:Fault><faultcode>s:Client</faultcode><faultstring>UPnPError</faultstring><detail>
 <UPnPError xmlns="urn:schemas-upnp-org:control-1-0"><errorCode>501</errorCode>
 <errorDescription>Invalid XML</errorDescription></UPnPError></detail></s:Fault></s:Body></s:Envelope> --}


 {-- SOAP Success
<?xml version="1.0" encoding="utf-8"?>
<s:Envelope s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
<s:Body>
<u:BrowseResponse xmlns:u="urn:schemas-upnp-org:service:ContentDirectory:1"><Result>&lt;DIDL-Lite xmlns:dc=&quot;http://purl.org/dc/elements/1.1/&quot; xmlns:upnp=&quot;urn:schemas-upnp-org:metadata-1-0/upnp/&quot; xmlns=&quot;urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/&quot; xmlns:dlna=&quot;urn:schemas-dlna-org:metadata-1-0/&quot;&gt;
&lt;container id=&quot;0\1\2&quot; parentID=&quot;0\1&quot; restricted=&quot;0&quot; searchable=&quot;1&quot; childCount=&quot;61&quot;&gt;
&lt;dc:title&gt;My Video&lt;/dc:title&gt;
&lt;upnp:class&gt;object.container&lt;/upnp:class&gt;
&lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.container&lt;/upnp:createClass&gt;
&lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.imageItem&lt;/upnp:createClass&gt;
&lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.audioItem&lt;/upnp:createClass&gt;
&lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.videoItem&lt;/upnp:createClass&gt;
&lt;dc:creator&gt;HUMAX&lt;/dc:creator&gt;
&lt;/container&gt;
&lt;/DIDL-Lite&gt;</Result>
<NumberReturned>1</NumberReturned>
<TotalMatches>1</TotalMatches>
<UpdateID>0</UpdateID>
</u:BrowseResponse>
  </s:Body>
</s:Envelope>}
--}
