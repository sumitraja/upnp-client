module Network.UPnP.Lib.XMLStuff (
    ToXML(..),
    FromXML(..),
    nameWithNS,
    XMLParseError,
    listHeadToEither,
    listToEither,
    XMLParser,
    extractContentOrError,
    extractElementOrError,
    extractConvertOrError,
    extractOptionalContent,
    xmlEncoded,
    elementMaker,
    mapRequiredAttribute,
    mapOptionalAttribute,
    chkName
) where

import ClassyPrelude
import Text.XML
import qualified Data.Text as T
import Text.XML.Cursor
import HTMLEntities.Decoder
import Data.Text.Lazy.Builder
import Data.Maybe
import Text.Read

class ToXML a where
  makeXML :: a -> Node

class FromXML a where
  fromXML :: Cursor -> a

type XMLParseError = T.Text
type XMLParser b = Cursor -> Either XMLParseError b
type ElementMaker = T.Text -> Name

elementMaker :: ElementMaker -> T.Text -> Axis
elementMaker f = element . f

nameWithNS :: T.Text -> T.Text -> Name
nameWithNS ns element = Name {nameLocalName = element, nameNamespace = Just ns, namePrefix = Nothing}

listHeadToEither :: XMLParseError -> [a] -> Either XMLParseError a
listHeadToEither msg [] = Left msg
listHeadToEither _ (value:values) = Right value

listToEither :: XMLParseError -> [a] -> Either XMLParseError [a]
listToEither msg [] = Left msg
listToEither _ values = Right values

chkName :: Cursor -> Name -> Either XMLParseError Cursor
chkName cur = chkit (node cur)
  where
  chkit node@(NodeElement elem) em =
    let
      name = elementName elem
      expected = em
      in (if  name == expected then Right cur else
        Left (tshow em <> " element not found:" <> tshow cur))
  chkit _ em = Left (tshow em <> " is not an element: " <> tshow cur)

extractElement :: Axis -> Cursor -> [Cursor]
extractElement axis parent = parent $/ axis

extractElementOrError :: (T.Text -> Axis) -> T.Text -> Cursor -> Either XMLParseError Cursor
extractElementOrError em elemstr parent = listHeadToEither (tshow elemstr <> " not parsed: " <> tshow parent)
  (extractElement (em elemstr) parent)

extractOptionalContent :: (T.Text -> Axis) -> T.Text -> Cursor -> Either XMLParseError (Maybe T.Text)
extractOptionalContent em elemstr parent = do
  let childE = extractElementOrError em elemstr parent
  case childE of
    Left err -> Right Nothing
    Right cur -> Right (listToMaybe (cur $/  content))

extractContentOrError :: (T.Text -> Axis) -> T.Text -> Cursor -> Either XMLParseError T.Text
extractContentOrError em elemstr parent = do
  child <- extractElementOrError em elemstr parent
  listHeadToEither (tshow  elemstr <> "content not extracted: " <> tshow child) (child $/ content)

extractConvertOrError :: Read a => (T.Text -> Axis) -> T.Text -> Cursor -> Either XMLParseError a
extractConvertOrError em elemstr parent = do
  txt <- extractContentOrError em elemstr parent
  (actual, _) <- listHeadToEither (tshow elemstr <> "contents not parsed: " <> tshow txt) (reads $ T.unpack txt)
  Right actual

mapRequiredAttribute ::  Name -> Cursor -> Either XMLParseError T.Text
mapRequiredAttribute attrName root = listHeadToEither (tshow attrName <> " not found in " <> tshow root) (root $| attribute attrName)

mapOptionalAttribute :: Name -> Cursor -> Maybe T.Text
mapOptionalAttribute attrName root = either (const Nothing) Just (mapRequiredAttribute attrName root)

xmlEncoded :: T.Text -> Cursor
xmlEncoded txt = fromDocument (parseText_ def xml)
  where
    xml = toLazyText $ htmlEncodedText txt
