
module Network.UPnP.Lib.DIDL (
  UPNPClass(..),
  UPNPItem(..),
  UPNPResource(..),
  DIDLElement(..),
  DIDLResponse,
  parseDIDL,
  processIteratively,
  processContainer,
  didlElementMaker
) where

import ClassyPrelude
import qualified Data.ByteString.Char8 as C
import qualified Data.CaseInsensitive as CI
import qualified Data.Map.Lazy as Map
import Network.UPnP.Lib.RFC2616
import Text.XML.Cursor
import Text.XML
import qualified Text.XML as XML
import qualified Data.Text as T
import Control.Applicative
import Network.UPnP.Lib.XMLStuff
import Data.Maybe
import Data.Map (Map)
import qualified Data.Map as Map

-- http://www.upnp.org/schemas/av/upnp.xsd

data UPNPClass = UPNPContainer | UPNPItem
data UPNPContainer = StorageContainer | UnknownContainer
data UPNPItem = AudioItem | VideoItem | ImageItem | UnknownItem String deriving (Eq)

instance Show UPNPClass where
  show c@UPNPContainer = show c
  show i@UPNPItem = show i

instance Show UPNPContainer where
  show StorageContainer = "object.container.storageFolder"
  show UnknownContainer = "object.container.*"

upnpContainerStringMap = Map.fromList [(show StorageContainer, StorageContainer)]

instance ToXML UPNPClass where
  makeXML (x@UPNPContainer) =  XML.NodeElement $ XML.Element (upnpMetadataNS "class") Map.empty [XML.NodeContent $ T.pack $ show x]
  makeXML (x@UPNPItem) = XML.NodeElement $ XML.Element (upnpMetadataNS "class") Map.empty [XML.NodeContent $ T.pack $ show x]

instance Show UPNPItem where
  show AudioItem = "object.item.audioItem"
  show VideoItem = "object.item.videoItem"
  show ImageItem = "object.item.imageItem"
  show (UnknownItem str) = str

upnpItemStringMap = Map.fromList [(show AudioItem, AudioItem), (show VideoItem, VideoItem),
  (show ImageItem, ImageItem), (show AudioItem ++ ".musicTrack", AudioItem),
  (show VideoItem ++ ".movie", VideoItem)]

upnpItemFromString :: String -> UPNPItem
upnpItemFromString str = Map.findWithDefault (UnknownItem str) str upnpItemStringMap

upnpContainerFromString :: String -> UPNPContainer
upnpContainerFromString str = Map.findWithDefault UnknownContainer str upnpContainerStringMap

data UPNPResource = Audio { duration:: T.Text, bitrate :: T.Text, sampleFrequency :: T.Text, nrAudioChannels :: Maybe T.Text,
  protocolInfo :: T.Text, url :: T.Text} | Video { duration:: T.Text, size :: T.Text, resolution :: Maybe T.Text,
  bitrate :: T.Text, nrAudioChannels :: Maybe T.Text, protocolInfo :: T.Text, url :: T.Text} | UnknownResource deriving (Show, Eq)

data DIDLElement = Container {id :: T.Text, title :: T.Text, creator :: T.Text, description :: Maybe T.Text} |
-- specialise this further to a video item and audio item - GADT?
  Item {id :: T.Text, title :: T.Text, creator :: T.Text, description :: Maybe T.Text, res :: UPNPResource, klass :: UPNPItem,
    seriesTitle :: Maybe T.Text, programTitle :: Maybe T.Text, episodeNumber :: Maybe T.Text, originalTrackNumber :: Maybe T.Text} deriving (Show, Eq)

type DIDLResponse = [DIDLElement]

parseDIDL :: Cursor -> Either XMLParseError DIDLResponse
parseDIDL cursor = do
    didlNode <- chkName cursor (didlNS "DIDL-Lite")
    let itemNodes = didlNode $/ didlElementMaker "item"
    let containerNodes = didlNode $/ didlElementMaker "container"
    items <- processIteratively processItem itemNodes
    containers <- processIteratively processContainer containerNodes
    return (containers ++ items)

processIteratively :: (Cursor -> Either XMLParseError DIDLElement) -> [Cursor] -> Either XMLParseError DIDLResponse
processIteratively _ [] = Right []
processIteratively f (cur:curs) = do
  item <- f cur
  nextItems <- processIteratively f curs
  return (item : nextItems)

processContainer :: Cursor -> Either XMLParseError DIDLElement
processContainer cur = do
  cid <- mapRequiredAttribute "id" cur
  title <- extractContentOrError dublinCoreElementMaker "title" cur
  creator <- extractContentOrError dublinCoreElementMaker "creator" cur
  description <- extractOptionalContent dublinCoreElementMaker "description" cur
  return (Container cid title creator description)

processItem :: Cursor -> Either XMLParseError DIDLElement
processItem item = do
  klassNode <- listToEither classError (item $/ laxElement "class" &// content)
  let kklass = upnpItemFromString $ T.unpack $ T.concat klassNode
  iId <- listHeadToEither iderror (item $| laxAttribute "id")
  iTitle <- extractContentOrError dublinCoreElementMaker "title" item
  iCreator <- extractContentOrError dublinCoreElementMaker "creator" item
  iDescription <- extractOptionalContent dublinCoreElementMaker "description" item
  resElem <- extractElementOrError didlElementMaker "res" item
  res <- createUPNPResource kklass resElem
  return (Item iId iTitle iCreator iDescription res kklass iSeriesTitle iProgramTitle iEpisodeNumber iOtn)
  where
    classError = "Item class not extracted: " <> tshow item
    iderror = "Item id not extracted: " <> tshow item
    iOtn = maybeElementContent item "originalTrackNumber"
    iSeriesTitle = maybeElementContent item "seriesTitle"
    iProgramTitle = maybeElementContent item "programTitle"
    iEpisodeNumber = maybeElementContent item "episodeNumber"

maybeElementContent :: Cursor -> T.Text -> Maybe T.Text
maybeElementContent item str = case item $// laxElement str &//content of
  list@(_ : _) -> Just $ T.concat list
  [] -> Nothing

extractCommonAttributes :: Cursor -> Either XMLParseError (T.Text, T.Text, Maybe T.Text, T.Text, T.Text)
extractCommonAttributes x = do
  dur <- mapRequiredAttribute "duration" x
  br  <- mapRequiredAttribute "bitrate" x
  let nACh = mapOptionalAttribute "nrAudioChannels" x
  pinfo  <- mapRequiredAttribute "protocolInfo" x
  url <- listHeadToEither ("Did not find item content in " <> tshow x) (x $// content)
  return (dur, br, nACh, pinfo, url)

createUPNPResource :: UPNPItem -> Cursor -> Either XMLParseError UPNPResource
-- <res duration="0:03:49.000" bitrate="40000" sampleFrequency="44100" nrAudioChannels="2" protocolInfo="http-get:*:audio/mpeg:DLNA.ORG_PN=MP3;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000">http://192.168.1.51:32469/object/fc5965c22024724c2814/file.mp3</res>
createUPNPResource AudioItem x = do
    (dur, br, nACh, pinfo, url) <- extractCommonAttributes x
    sFreq  <- mapRequiredAttribute "sampleFrequency" x
    return (Audio dur br sFreq nACh pinfo url)

-- <res duration="0:56:27.000" size="368589256" resolution="624x352" bitrate="108875" nrAudioChannels="2" protocolInfo="http-get:*:video/x-msvideo:DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000">http://192.168.1.51:32469/object/50acb9207e2812a0cb96/file.avi</res>
createUPNPResource VideoItem x  = do
  (dur, br, nACh, pinfo, url) <- extractCommonAttributes x
  sz <- mapRequiredAttribute "size" x
  let res = mapOptionalAttribute "resolution" x
  return (Video dur sz res br nACh pinfo url)

createUPNPResource _ _ = Right UnknownResource

elementName :: XML.Node -> Maybe T.Text
elementName (XML.NodeElement elem) = Just $ XML.nameLocalName $ XML.elementName elem
elementName _ = Nothing

didlNS :: T.Text -> XML.Name
didlNS = nameWithNS  "urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/"

dublinCoreNS :: T.Text -> XML.Name
dublinCoreNS = nameWithNS "http://purl.org/dc/elements/1.1/"

upnpMetadataNS :: T.Text -> XML.Name
upnpMetadataNS = nameWithNS "urn:schemas-upnp-org:metadata-1-0/upnp/"

dlnaNS :: T.Text -> XML.Name
dlnaNS = nameWithNS "urn:schemas-dlna-org:metadata-1-0/"

didlElementMaker = element . didlNS

dublinCoreElementMaker = element . dublinCoreNS
