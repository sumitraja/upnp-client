{-# LANGUAGE OverloadedStrings, FlexibleInstances #-}

module Network.UPnP.Lib.DeviceDiscovery (
  UPNPDevice(..),
  DeviceSpec(..),
  DeviceService(..),
  parseUPNPDiscovery,
  parseDeviceServices
) where

import ClassyPrelude
import qualified Data.CaseInsensitive as CI
import Network.UPnP.Lib.RFC2616
import Text.XML.Cursor
import Text.XML
import qualified Data.Text as T
import Control.Applicative
import Network.UPnP.Lib.XMLStuff
import Data.Text.Encoding
import qualified Data.ByteString as B

type URL = T.Text
type UPNPResponse = B.ByteString

data UPNPDevice = UPNPDevice { usn :: T.Text, location :: URL} deriving (Show, Eq)
data DeviceSpec = DeviceSpec { deviceType :: T.Text, friendlyName :: T.Text, udn :: T.Text,
    services :: [DeviceService]} deriving (Show, Eq)
data DeviceService = DeviceService {serviceType :: T.Text, serviceId :: T.Text, scpdURL :: T.Text,
    controlURL :: T.Text, eventSubURL :: T.Text} deriving (Show, Eq)

parseUPNPDiscovery :: UPNPResponse -> Either Text UPNPDevice
parseUPNPDiscovery resp = parseUPNPDiscoveryResponse $ parseToResponse resp

parseUPNPDiscoveryResponse :: Either Text (Response, [Header]) -> Either Text UPNPDevice
parseUPNPDiscoveryResponse (Right (Response {responseCode=rc}, headers)) = case T.unpack rc of
        "200" -> getDiscoveryEither headers
        x -> Left ("Response code of " <> tshow x)
parseUPNPDiscoveryResponse (Left str) = Left str

getDiscoveryEither :: [Header] -> Either Text UPNPDevice
getDiscoveryEither headers = let
        devusn =  fromMaybe "" (listToMaybe (extractHeader headers (T.pack "USN")))
        devlocation = headMay $ extractHeader headers (T.pack "Location")
    in
        case devlocation of
            Just loc -> Right $ UPNPDevice devusn loc
            Nothing -> Left "Missing location in response"

extractHeader :: [Header] -> T.Text -> [T.Text]
extractHeader h nameToFind = concatMap headerValue res
  where
    finder hdr = CI.mk (headerName hdr) == CI.mk nameToFind
    res = filter finder h

-- parseDevice :: String -> DeviceSpec
-- parseDevice str = parseDeviceServices $ fromDocument $ parseText_ def $ T.pack str
--
-- serviceType :: String, serviceId :: String, scpdURL :: String, controlURL :: String, eventSubURL :: String
parseDeviceService :: Cursor -> Either XMLParseError DeviceService
parseDeviceService servicesNode = do
  serviceType <- extractContentOrError laxElement "serviceType" servicesNode
  serviceId <- extractContentOrError laxElement "serviceId" servicesNode
  scpdURL <- extractContentOrError laxElement "SCPDURL" servicesNode
  controlURL <- extractContentOrError laxElement "controlURL" servicesNode
  eventSubURL <- extractContentOrError laxElement "eventSubURL" servicesNode
  return (DeviceService serviceType serviceId scpdURL controlURL eventSubURL)

parseDeviceServices :: Cursor -> Either XMLParseError [DeviceService]
parseDeviceServices cursor = mapM parseDeviceService servicesNodes
  where
    servicesNodes = cursor $/ laxElement "device" &/ laxElement "serviceList" &// laxElement "service"
