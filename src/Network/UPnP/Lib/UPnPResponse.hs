module Network.UPnP.Lib.UPnPResponse (
    parseUPNPError,
    upnpControlNS,
    UPNPError(..),
    parseUPNPBrowseResponse,
    UPNPBrowseResponse(..)
) where

import ClassyPrelude
import Network.UPnP.Lib.UPnPSOAP
import Network.UPnP.Lib.XMLStuff
import Text.XML
import Text.XML.Cursor
import qualified Data.Text as T
import Network.UPnP.Lib.DIDL
import Network.UPnP.Lib.UPnPCommands (contentDirectoryNS)

data UPNPError = UPNPError { errorCode :: Int, errorDescription :: T.Text} deriving (Eq, Show)

data UPNPBrowseResponse a = UPNPBrowseResponse { result :: a, numberReturned :: Int, totalMatches :: Int, updateId :: T.Text}

upnpControlNS :: T.Text -> Name
upnpControlNS = nameWithNS "urn:schemas-upnp-org:control-1-0"

parseUPNPError :: SOAPErrorParser UPNPError
parseUPNPError cur = do
  upnpError <- extractElementOrError elementMaker "UPnPError" cur
  errorCode <- extractConvertOrError elementMaker "errorCode" upnpError
  errorDescription <- extractContentOrError elementMaker "errorDescription" upnpError
  return (UPNPError errorCode errorDescription)
  where
      elementMaker = element . upnpControlNS


parseUPNPBrowseResponse :: XMLParser a -> SOAPBodyParser (UPNPBrowseResponse a)
parseUPNPBrowseResponse parser cur = do
  browseResponse <- chkName cur (contentDirectoryNS "BrowseResponse")
  numberReturned <- extractConvertOrError laxElement "NumberReturned" browseResponse
  totalMatches <- extractConvertOrError laxElement "TotalMatches" browseResponse
  updateId <- extractContentOrError laxElement "UpdateID" browseResponse
  didlText <- extractContentOrError laxElement "Result" browseResponse
  obj <- parser $ xmlEncoded didlText
  return (UPNPBrowseResponse obj numberReturned totalMatches updateId)
  where
    elementMaker = element . contentDirectoryNS


{--
<u:BrowseResponse xmlns:u="urn:schemas-upnp-org:service:ContentDirectory:1"><Result>&lt;DIDL-Lite xmlns:dc=&quot;http://purl.org/dc/elements/1.1/&quot; xmlns:upnp=&quot;urn:schemas-upnp-org:metadata-1-0/upnp/&quot; xmlns=&quot;urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/&quot; xmlns:dlna=&quot;urn:schemas-dlna-org:metadata-1-0/&quot;&gt;
&lt;container id=&quot;0\1\2&quot; parentID=&quot;0\1&quot; restricted=&quot;0&quot; searchable=&quot;1&quot; childCount=&quot;61&quot;&gt;
&lt;dc:title&gt;My Video&lt;/dc:title&gt;
&lt;upnp:class&gt;object.container&lt;/upnp:class&gt;
&lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.container&lt;/upnp:createClass&gt;
&lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.imageItem&lt;/upnp:createClass&gt;
&lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.audioItem&lt;/upnp:createClass&gt;
&lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.videoItem&lt;/upnp:createClass&gt;
&lt;dc:creator&gt;HUMAX&lt;/dc:creator&gt;
&lt;/container&gt;
&lt;/DIDL-Lite&gt;</Result>
<NumberReturned>1</NumberReturned>
<TotalMatches>1</TotalMatches>
<UpdateID>0</UpdateID>
</u:BrowseResponse>
--}
