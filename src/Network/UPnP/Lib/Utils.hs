{-# LANGUAGE OverloadedStrings #-}

module Network.UPnP.Lib.Utils (
  printErr,
  matchDeviceByIP,
  upnpBroadcast,
  upnpBroadcastHost,
  mtu,
  port,
  CommandHandler
) where

import ClassyPrelude
import System.Console.ANSI
import System.IO
import Network.UPnP.Lib.DeviceDiscovery
import Control.Monad
import Data.Maybe
import Network.URI
import qualified Data.ByteString.Char8 as C
import Network.Socket (PortNumber)
import Data.Text.IO as TIO

type CommandHandler = [UPNPDevice] -> IO ()

mtu :: Int
mtu=1500

port :: PortNumber
port = 1900

upnpBroadcastHost :: String
upnpBroadcastHost = "239.255.255.250"

upnpBroadcast :: C.ByteString
upnpBroadcast = "M-SEARCH * HTTP/1.1\r\n\
\HOST: 239.255.255.250:1900\r\n\
\MAN: \"ssdp:discover\"\r\n\
\MX: 3\r\n\
\ST: ssdp:all\r\n\
\\r\n"

printErr :: Text -> IO ()
printErr str = do
  ansi <- hSupportsANSI stderr
  when ansi (hSetSGR stderr [SetColor Foreground Vivid Red])
  TIO.hPutStrLn stderr str
  when ansi (hSetSGR stderr [Reset])

matchDeviceByIP ::  Text -> UPNPDevice -> Bool
matchDeviceByIP host dev = isJust checkURI
  where
    checkURI :: Maybe String
    checkURI = do
      uri <- parseURI (unpack (location dev))
      uriAuth <- uriAuthority uri
      let reg = uriRegName uriAuth
      if reg == unpack host then return reg else Nothing
