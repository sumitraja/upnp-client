{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Network.UPnP.Lib.UPnPCommands (
    Command(..),
    BrowseFlag(..),
    Filter(..),
    SortField(..),
    ToXML(..),
    contentDirectoryNS,
    contentDirectorySchema
) where

import qualified Data.Text as T
import Text.XML
import Text.XML.Cursor
import Text.Hamlet.XML
import Data.Map (empty)
import Network.UPnP.Lib.XMLStuff (ToXML, makeXML, nameWithNS)
import Network.UPnP.Lib.UPnPSOAP
import ClassyPrelude

{--
SOAPACTION:"urn:schemas-upnp-org:service:ContentDirectory:1#Browse"
<u:Browse xmlns:u="urn:schemas-upnp-org:service:ContentDirectory:1">
<ObjectID>0</ObjectID>
      <BrowseFlag>BrowseDirectChildren</BrowseFlag>
      <Filter>*</Filter>
      <StartingIndex>0</StartingIndex>
      <RequestedCount>0</RequestedCount>
      <SortCriteria></SortCriteria>
-}

data UPNPCommand = UPNPCommand {soapAction :: T.Text, command::Command}

data Command = Browse {objectID::T.Text, browseFlag::BrowseFlag, filter::Filter, startingIndex::Integer,
    requestedCount::Integer, sortCriteria::[SortField]}

data BrowseFlag = BrowseMetadata | BrowseDirectChildren deriving (Show)

data Filter = All | Property T.Text

instance Show Filter where
  show All = "*"
  show (Property f) = show f

data SortField = Asc T.Text | Desc T.Text
instance Show SortField where
  show (Asc p) = "+" ++ T.unpack p
  show (Desc p) = "-" ++ T.unpack p

contentDirectorySchema :: T.Text
contentDirectorySchema = "urn:schemas-upnp-org:service:ContentDirectory:1"

contentDirectoryNS :: T.Text -> Name
contentDirectoryNS  = nameWithNS contentDirectorySchema

instance ToXML Command where
  makeXML (x@Browse{}) = browseXML x

instance SoapActionProvider Command where
  getSoapAction Browse{} = "urn:schemas-upnp-org:service:ContentDirectory:1#Browse"

browseXML :: Command -> Node
browseXML (b@Browse{}) = NodeElement $ Element (contentDirectoryNS "Browse") Data.Map.empty [xml|
<ObjectID>#{objectID b}
<BrowseFlag>#{T.pack $ show $ browseFlag b}
<Filter>#{T.pack $ show $ Network.UPnP.Lib.UPnPCommands.filter b}
<StartingIndex>#{T.pack $ show $ startingIndex b}
<RequestedCount>#{T.pack $ show $ requestedCount b}
<SortCriteria>#{T.pack $ intercalate "," (fmap show (sortCriteria b))}
|]
