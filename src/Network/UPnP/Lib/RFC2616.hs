{-# LANGUAGE OverloadedStrings #-}

module Network.UPnP.Lib.RFC2616
    (
      Header(..)
    , Request(..)
    , Response(..)
    , request
    , response
    , parseToResponse
    ) where

import ClassyPrelude
import Control.Applicative
import Data.Attoparsec.Text as P
import Data.Attoparsec.Text (char, endOfLine, isEndOfLine, isHorizontalSpace)
import Data.Char (isDigit, chr)
import Data.ByteString ()
import Data.Word (Word8)
import Data.Text as T

isToken :: Char -> Bool
isToken w = w <= chr 127 && notInClass "\0-\31()<>@,;:\\\"/[]?={} \t" w

skipSpaces :: Parser ()
skipSpaces = satisfy isHorizontalSpace *> skipWhile isHorizontalSpace

data Request = Request {
      requestMethod  :: Text
    , requestUri     :: Text
    , requestVersion :: Text
    } deriving (Eq, Ord, Show)

httpVersion :: Parser Text
httpVersion = "HTTP/" *> P.takeWhile (\c -> isDigit c || c == chr 46)

requestLine :: Parser Request
requestLine = Request <$> (takeWhile1 isToken <* char ' ')
                      <*> (takeWhile1 (/= chr 32) <* char ' ')
                      <*> (httpVersion <* endOfLine)

data Header = Header {
      headerName  :: Text
    , headerValue :: [Text]
    } deriving (Eq, Ord, Show)

messageHeader :: Parser Header
messageHeader = Header
  <$> (P.takeWhile isToken <* char ':' <* skipWhile isHorizontalSpace)
  <*> ((:) <$> (takeTill isEndOfLine <* endOfLine)
           <*> many (skipSpaces *> takeTill isEndOfLine <* endOfLine))

request :: Parser (Request, [Header])
request = (,) <$> requestLine <*> many messageHeader <* endOfLine

data Response = Response {
      responseVersion :: Text
    , responseCode    :: Text
    , responseMsg     :: Text
    } deriving (Eq, Ord, Show)

responseLine :: Parser Response
responseLine = Response <$> (httpVersion <* char ' ')
                        <*> (P.takeWhile isDigit <* char ' ')
                        <*> (takeTill isEndOfLine <* endOfLine)

response :: Parser (Response, [Header])
response = (,) <$> responseLine <*> many messageHeader <* endOfLine

parseToResponse :: ByteString -> Either Text (Response, [Header])
parseToResponse msg = case parseOnly response (decodeUtf8 msg) of
  Left msg -> Left (T.pack msg)
  Right rm -> Right rm
