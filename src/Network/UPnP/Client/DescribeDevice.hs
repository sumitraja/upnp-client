{-# LANGUAGE OverloadedStrings #-}

module Network.UPnP.Client.DescribeDevice (
  describeHandler,
  getURLToDeviceService
) where

import ClassyPrelude
import Network.UPnP.Lib.DeviceDiscovery
import Network.URI
import qualified Data.ByteString.Char8 as C
import Text.XML
import Text.XML.Cursor
import Network.HTTP.Conduit as HTTP
import Data.Either.Combinators
import Network.UPnP.Lib.XMLStuff
import Data.Maybe
import Network.UPnP.Lib.Utils
import qualified Data.Text as T

describeHandler :: T.Text -> [UPNPDevice] -> IO ()
describeHandler host devs = case find (matchDeviceByIP host) devs of
    Just dev -> getURLToDeviceService (location dev) >>= printDeviceServices
    Nothing -> printErr ("Host " ++ host ++ " not found from UPnP browse")

printDeviceServices :: Either XMLParseError [DeviceService] -> IO()
printDeviceServices (Left error) = printErr ("Failed to process: " <> tshow error)
printDeviceServices (Right lst@(_:_)) = mapM_ print lst
printDeviceServices (Right []) = printErr "No device services found"

getURLToDeviceService :: T.Text -> IO (Either XMLParseError [DeviceService])
getURLToDeviceService jurl = do
  httpcontent <- simpleHttp (T.unpack jurl)
  return (parseDeviceServices (fromDocument $ parseText_ def  (decodeUtf8 httpcontent)))
