{-# LANGUAGE OverloadedStrings #-}

module Network.UPnP.Client.ContentDirectory (
  contentDirectoryHandler
) where

import ClassyPrelude
import Network.UPnP.Lib.DeviceDiscovery
import Network.UPnP.Lib.UPnPCommands as Cmd
import Network.URI
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString.Lazy as BL
import Text.XML
import Text.XML.Cursor
import Network.HTTP.Conduit as HTTP
import Network.UPnP.Client.DescribeDevice
import Network.UPnP.Lib.Utils
import Network.UPnP.Lib.XMLStuff
import Network.UPnP.Lib.DIDL
import Network.UPnP.Lib.UPnPSOAP
import Network.UPnP.Lib.UPnPResponse
import Data.Map
import qualified Network.HTTP.Types as HTypes
import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Encoding as LTE
import Network.UPnP.Client.Output

contentDirectoryHandler :: Text -> Text -> Maybe String -> [UPNPDevice] -> IO ()
contentDirectoryHandler objectId host outputString devs = case find (matchDeviceByIP host) devs of
    Just dev -> do
      devices <- getURLToDeviceService (location dev)
      let f = maybe outputItems (outputItemsWithTemplate . pack) outputString
      processDeviceServices objectId (location dev) f devices
    Nothing -> printErr ("Host " <> host <> " not found from UPnP browse")

processDeviceServices :: Text -> Text -> (DIDLResponse -> [Text]) -> Either XMLParseError [DeviceService] -> IO()
processDeviceServices _ _ _ (Left error) = printErr $ "Failed to process: " ++ error
processDeviceServices objectId url outputer (Right devices@(_:_)) = case filterContentDirectory devices of
    Just device -> do
      e <- makeBrowseRequest objectId url device
      case e of
        Right didl -> mapM_ putStrLn (outputer didl)
        Left (RequestError status err) -> printErr (tshow status <> ":" <> err)
        Left (RequestSOAPError status sfault) -> printErr (tshow status <> ":" <> tshow sfault)
    Nothing -> printErr "Host does not have a ContentDirectory service"
processDeviceServices _ _ _ (Right []) = printErr "No device services found"

makeBrowseRequest :: Text -> Text -> DeviceService -> IO (Either RequestError DIDLResponse)
makeBrowseRequest objectId url dev = do
  trequest <- parseUrl ("POST " ++ unpack cdControlUrl)
  let request = createSoapRequest trequest browseCmd
  -- let (RequestBodyLBS lbs) = requestBody request
  -- print (decodeUtf8 lbs)
  manager <- newManager tlsManagerSettings
  res <- httpLbs request manager
  -- print (LTE.decodeUtf8 (HTTP.responseBody res))
  return (parseResponse res)
  where
    cdControlUrl = url <> controlURL dev
    browseCmd = Browse objectId BrowseDirectChildren Cmd.All 0 1000 []

data RequestError = RequestSOAPError HTypes.Status (SOAPFault UPNPError) | RequestError HTypes.Status Text deriving (Show)

parseResponse :: HTTP.Response BL.ByteString -> Either RequestError DIDLResponse
parseResponse res = do
  let env = parseSOAPEnvelope doco parseUPNPError (parseUPNPBrowseResponse parseDIDL)
  case env of
    Left err -> Left (RequestError (HTTP.responseStatus res) err)
    Right soapenv -> checkSOAPEnvelope (HTTP.responseStatus res) soapenv
  where
    text = LTE.decodeUtf8 (HTTP.responseBody res)
    doco = fromDocument (parseText_ def text)
    checkSOAPEnvelope :: HTypes.Status -> SOAPEnvelope UPNPError (UPNPBrowseResponse DIDLResponse) -> Either RequestError DIDLResponse
    checkSOAPEnvelope status (SOAPFaultEnvelope sfault) = Left (RequestSOAPError status sfault)
    checkSOAPEnvelope _ (SOAPEnvelope UPNPBrowseResponse {result = didl}) = Right didl

filterContentDirectory :: [DeviceService] -> Maybe DeviceService
filterContentDirectory = find (\ds -> serviceType ds == contentDirectorySchema)
