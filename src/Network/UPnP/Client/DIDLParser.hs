{-# LANGUAGE OverloadedStrings #-}

module Network.UPnP.Client.DIDLParser (
  parseFile
) where

import ClassyPrelude
import Network.UPnP.Lib.DIDL
import qualified Text.XML as XML
import Text.XML.Cursor
import Network.UPnP.Lib.XMLStuff
import Network.UPnP.Client.Output
import Network.UPnP.Lib.Utils

parseFile :: String -> Maybe String -> IO ()
parseFile file plate = do
  doc <- XML.readFile XML.def file
  let res = parseDIDL (fromDocument doc)
  let f = maybe outputItems (outputItemsWithTemplate . pack) plate
  case res of
    Left err -> printErr (tshow err)
    Right res -> mapM_ putStrLn (f res)
