{-# LANGUAGE OverloadedStrings #-}

module Network.UPnP.Client.UPnPClient (
  optionHandler,
  parseOptions,
  withInfo
)where

import ClassyPrelude
import Network.Socket.ByteString
import Network.Socket hiding (recv, sendTo)
import Network.URI
import Control.Monad (forever, forM_)
import qualified Data.ByteString.Char8 as C
import Network.UPnP.Lib.DeviceDiscovery
import Options.Applicative as OA
import Network.UPnP.Client.DescribeDevice
import Network.UPnP.Client.DIDLParser
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Data.Maybe
import Network.UPnP.Lib.Utils
import Data.Either
import Network.UPnP.Client.ContentDirectory

type ObjectId = String
type Filename = String

data Options = Options Command (Maybe Int)
data Command = ContentDir HostName (Maybe ObjectId) (Maybe String)| Device HostName | Browse | ParseDIDL Filename (Maybe String)

parseOptions :: Parser Options
parseOptions = Options <$> parseCommand <*> parseScanTimeout

parseScanTimeout :: Parser (Maybe Int)
parseScanTimeout = optional $ option auto $
    short 't' OA.<> long "timeout" OA.<> metavar "N" OA.<> help "UPNP scan time out in seconds (default 5)"

parseDIDLFile :: Parser Filename
parseDIDLFile = strOption $
    short 'f' OA.<> long "file" OA.<> metavar "FILENAME" OA.<> help "Filename of DIDL to parse"

parseOutputString :: Parser (Maybe String)
parseOutputString = optional $ strOption $
    short 'o' OA.<> long "output-string" OA.<> metavar "STRING" OA.<> help "String to output for each DIDL Item. Available vars are  $url, $title. Containers are ignored if this is specified"

parseDIDL :: Parser Command
parseDIDL = ParseDIDL <$> parseDIDLFile <*> parseOutputString

parseIP :: Parser HostName
parseIP = strOption $
    short 'i' OA.<> long "ipaddress" OA.<> metavar "IP-ADDRESS" OA.<> help "Target IP of device (without port)"

parseContentDir :: Parser Command
parseContentDir = ContentDir <$> parseIP <*> optional (argument str (metavar "OBJECT-ID")) <*> parseOutputString

parseDevice :: Parser Command
parseDevice = Device <$> parseIP

parseBrowse :: Parser Command
parseBrowse = pure Browse

parseCommand :: Parser Command
parseCommand = subparser
  ( command "browse" (parseBrowse `withInfo` "Browse and list UPNP devices. <Enter> to quit" ) OA.<>
    command "list" (parseContentDir `withInfo` "List items given the optional object-id or root" ) OA.<>
    command "describe" (parseDevice `withInfo` "Describe the given device" ) OA.<>
    command "parse" (parseDIDL `withInfo` "Parse the given DIDL")
  )

withInfo :: Parser a -> String -> ParserInfo a
withInfo opts desc = info (helper <*> opts) $ progDesc desc

defaultTimeout :: Maybe Int -> Int
defaultTimeout tm = fromMaybe 5 tm * 1000000

optionHandler :: Options -> IO()
optionHandler (Options Browse timeout) = socketRunner (defaultTimeout timeout) browseHandler
optionHandler (Options (Device hostname) timeout) = socketRunner (defaultTimeout timeout) (describeHandler (pack hostname))
optionHandler (Options (ContentDir hostname maybeObjectId maybeOutputString) timeout) = socketRunner (defaultTimeout timeout) (contentDirectoryHandler (maybe "0" pack maybeObjectId) (pack hostname) maybeOutputString)
optionHandler (Options (ParseDIDL filename plate) _) = parseFile filename plate

socketRunner :: Int -> CommandHandler -> IO()
--socketRunner (Main.deviceurl -> Just jurl) = simpleHttp jurl >>= return . parseDeviceServices . fromDocument . parseText_ def . decodeUtf8 >>= print
socketRunner timeout handler = withSocketsDo $ do
  mvar <- newMVar []
  s <- socket AF_INET Datagram defaultProtocol
  hostAddr <- inet_addr upnpBroadcastHost
  threadId <- fork $ receiveMessages mvar s
  sendTo s upnpBroadcast (SockAddrInet port hostAddr)
  threadDelay timeout
  killThread threadId
  sClose s
  devices <- takeMVar mvar
  if null devices then printErr "No devices found" else handler devices

receiveMessages :: MVar [UPNPDevice] -> Socket -> IO ()
receiveMessages mvar socket = do
  msg <-recv socket mtu
  let deviceEither = parseUPNPDiscovery msg
  case deviceEither of
    (Left s) -> printErr ("Failed to parse response: " ++ s ++ ". Received ---> " ++ decodeUtf8 msg)
    (Right device) -> takeMVar mvar >>= \lst -> putMVar mvar (device : lst)
  receiveMessages mvar socket

browseHandler :: CommandHandler
browseHandler = mapM_ (\device -> putStrLn $ "Found " ++ usn device ++ " at " ++ location device)
