{-# LANGUAGE OverloadedStrings, RecordWildCards #-}

module Network.UPnP.Client.Output (
  outputItems,
  outputItemsWithTemplate
) where

import ClassyPrelude
import Network.UPnP.Lib.DIDL
import qualified Text.XML as XML
import Text.XML.Cursor
import qualified Data.ByteString.Char8 as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import Network.UPnP.Lib.XMLStuff
import Data.Text.Template
import Control.Monad

outputItemsWithTemplate :: T.Text -> DIDLResponse -> [T.Text]
outputItemsWithTemplate plate didl = fmap fmapper (filter findItems didl)
  where
    findItems :: DIDLElement -> Bool
    findItems Container{..} = False
    findItems Item{..} = True
    localContext :: DIDLElement -> T.Text -> T.Text
    localContext e s = case s of
      "url" -> url (res e)
      "title" -> title e
      _ -> ""
    fmapper :: DIDLElement -> T.Text
    fmapper e = TL.toStrict $ substitute plate (localContext e)

outputItems :: DIDLResponse -> [Text]
outputItems = fmap mapItems
  where
    mapItems :: DIDLElement -> Text
    mapItems Container{id = i, title = t} = "Container|" <> i <> "|" <> t
    mapItems Item{res = e, title = t} = "Item|" <> resType e <> "|" <> url e <> "|" <> t
    resType :: UPNPResource -> Text
    resType Audio{..} = "Audio"
    resType Video{..} = "Video"
