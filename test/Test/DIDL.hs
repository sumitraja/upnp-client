{-# LANGUAGE OverloadedStrings #-}

module Test.DIDL where

import Test.Framework (testGroup, Test)
import Test.Framework.Providers.HUnit
import Test.HUnit hiding (Test)
import Network.UPnP.Lib.DIDL as DIDL
import qualified Text.XML as XML
import Text.XML.Cursor
import qualified Data.ByteString.Char8 as C
import qualified Data.Text.Lazy as T
import Network.UPnP.Lib.XMLStuff
import ClassyPrelude

didlSuite :: Test
didlSuite = testGroup "Parse DIDL"
   [testCase "Parse DIDL Container" testParseDIDLContainer,
   testCase "Parse DIDL" testParseDIDL]

expectedDIDLContainerElements = Right [Container {DIDL.id = "94467912-bd40-4d2f-ad25-7b8423f7b05a", title = "Video", creator = "Unknown", description = Just "Video"},
  Container {DIDL.id = "abe6121c-1731-4683-815c-89e1dcd2bf11", title = "Music", creator = "Unknown", description = Just "Music"},
  Container {DIDL.id = "b0184133-f840-4a4f-a583-45f99645edcd", title = "Photos", creator = "Unknown", description = Just "Photos"}]

processContainers = processIteratively processContainer

testParseDIDLContainer :: Assertion
testParseDIDLContainer = do
   doc <- XML.readFile XML.def "sample/didl-container.xml"
   let cursor = fromDocument doc
   let didlNodes = cursor $/ didlElementMaker "container"
   processContainers didlNodes @?= expectedDIDLContainerElements

expectedDIDLElements = Right [Container {DIDL.id = "94467912-bd40-4d2f-ad25-7b8423f7b05a", title = "Video", creator = "Unknown", description = Just "Video"},
  Container {DIDL.id = "abe6121c-1731-4683-815c-89e1dcd2bf11", title = "Music", creator = "Unknown", description = Just "Music"},
  Container {DIDL.id = "b0184133-f840-4a4f-a583-45f99645edcd", title = "Photos", creator = "Unknown", description = Just "Photos"},
  Item {DIDL.id = "4b66bb5087f6f4dbcd79", title = "Stick Up", creator = "The Baker Brothers", description = Just "Stick Up",
    seriesTitle = Nothing, programTitle = Nothing, episodeNumber = Nothing, originalTrackNumber = Just "9",
    res = Audio {duration = "0:03:24.000", bitrate = "40000", sampleFrequency = "44100", nrAudioChannels = Just "2", protocolInfo = "http-get:*:audio/mpeg:DLNA.ORG_PN=MP3;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000",
      url = "http://192.168.1.51:32469/object/4b66bb5087f6f4dbcd79/file.mp3"}, klass = AudioItem},
  Item {DIDL.id = "fc5965c22024724c2814", title = "Hang Loose", creator = "The Baker Brothers", description = Just "Hang Loose",
    seriesTitle = Nothing, programTitle = Nothing, episodeNumber = Nothing, originalTrackNumber = Just "10",
    res = Audio {duration = "0:03:49.000", bitrate = "40000", sampleFrequency = "44100", nrAudioChannels = Just "2", protocolInfo = "http-get:*:audio/mpeg:DLNA.ORG_PN=MP3;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000",
      url = "http://192.168.1.51:32469/object/fc5965c22024724c2814/file.mp3"}, klass = AudioItem},
  Item {DIDL.id = "50acb9207e2812a0cb96", title = "S01E01 - Pilot", creator = "Vince Gilligan", description = Just "S01E01 - Pilot",
    programTitle = Just "S01E01 - Pilot", seriesTitle =  Just "Breaking Bad", episodeNumber= Just "1", originalTrackNumber = Nothing,
    res = Video {duration = "0:56:27.000", size = "368589256", resolution= Just "624x352", bitrate="108875", nrAudioChannels= Just "2", protocolInfo="http-get:*:video/x-msvideo:DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000",
      url = "http://192.168.1.51:32469/object/50acb9207e2812a0cb96/file.avi"}, klass = VideoItem}]

testParseDIDL :: Assertion
testParseDIDL = do
    doc <- XML.readFile XML.def "sample/didl-container-and-items.xml"
    let cursor = fromDocument doc
    parseDIDL cursor @?= expectedDIDLElements
