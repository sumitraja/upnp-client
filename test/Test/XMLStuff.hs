{-# LANGUAGE OverloadedStrings #-}

module Test.XMLStuff where

import Test.Framework (testGroup, Test)
import Test.Framework.Providers.HUnit
import Test.HUnit hiding (Test)
import Network.UPnP.Lib.UPnPSOAP
import Text.XML
import Text.XML.Cursor
import Text.Hamlet.XML
import qualified Data.ByteString.Char8 as C
import qualified Data.Text as T
import qualified Data.Map.Lazy as Map
import Network.UPnP.Lib.XMLStuff
import Network.UPnP.Lib.UPnPResponse (upnpControlNS)
import ClassyPrelude

xmlSuite :: Test
xmlSuite = testGroup "XML functions"
   [testCase "Extract child element content" testExtractElementContentOrError]

testXML = "<detail><UPnPError xmlns=\"urn:schemas-upnp-org:control-1-0\"><errorCode>501</errorCode><errorDescription>Invalid XML</errorDescription></UPnPError></detail>"
found = Right "501"

parse :: Cursor -> Either XMLParseError T.Text
parse cur = do
  detail <- chkName cur Name {nameLocalName = "detail", nameNamespace = Nothing, namePrefix = Nothing}
  upnpError <- extractElementOrError em "UPnPError" detail
  extractContentOrError em "errorCode" upnpError
  where
    em = elementMaker upnpControlNS

testExtractElementContentOrError :: Assertion
testExtractElementContentOrError = parse (fromDocument (parseText_ def testXML)) @?= found
