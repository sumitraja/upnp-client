{-# LANGUAGE OverloadedStrings #-}

module Test.UPnPCommands where

import Test.Framework (testGroup, Test)
import Test.Framework.Providers.HUnit
import Test.HUnit hiding (Test)
import Network.UPnP.Lib.UPnPCommands
import Text.XML as XML
import Text.XML.Cursor
import Text.Hamlet.XML
import qualified Data.ByteString.Char8 as C
import qualified Data.Text as T
import qualified Data.Map.Lazy as Map
import Network.UPnP.Lib.XMLStuff (ToXML, makeXML)
import Network.UPnP.Lib.UPnPSOAP
import ClassyPrelude

upnpCommandSuite :: Test
upnpCommandSuite = testGroup "UPNP Commands"
   [testCase "Create UPNP Browse" testCreateBrowseCommand,
    testCase "Check SOAP Action for Browse" testSOAPActionBrowse]

browseXML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\
\<test>\
\<u:Browse xmlns:u=\"urn:schemas-upnp-org:service:ContentDirectory:1\">\
\<ObjectID>0</ObjectID>\
\<BrowseFlag>BrowseDirectChildren</BrowseFlag>\
\<Filter>*</Filter>\
\<StartingIndex>0</StartingIndex>\
\<RequestedCount>10</RequestedCount>\
\<SortCriteria>+field,-field1</SortCriteria>\
\</u:Browse>\
\</test>"

testCreateBrowseCommand :: Assertion
testCreateBrowseCommand = createBrowseTest @?= documentRoot (parseText_ def browseXML)

createBrowseTest :: XML.Element
createBrowseTest = Element "test" Map.empty [makeXML $ Browse "0" BrowseDirectChildren All 0 10 [Asc "field", Desc "field1"]]

testSOAPActionBrowse :: Assertion
testSOAPActionBrowse = getSoapAction browseCmd @?= "urn:schemas-upnp-org:service:ContentDirectory:1#Browse"
  where
    browseCmd = Browse "0" BrowseDirectChildren All 0 10 [Asc "field", Desc "field1"]
