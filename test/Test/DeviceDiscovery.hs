{-# LANGUAGE OverloadedStrings #-}

module Test.DeviceDiscovery where

import Test.Framework (testGroup, Test)
import Test.Framework.Providers.HUnit
import Test.HUnit hiding (Test)
import Network.UPnP.Lib.DeviceDiscovery
import Text.XML
import Text.XML.Cursor
import qualified Data.ByteString.Char8 as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as LT
import Network.UPnP.Lib.XMLStuff
import ClassyPrelude

deviceXML :: LT.Text
deviceXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
\<root xmlns=\"urn:schemas-upnp-org:device-1-0\" xmlns:dlna=\"urn:schemas-dlna-org:device-1-0\" \
\    xmlns:hmx=\"urn:schemas-humax-co-kr:metadata-1-0\">\
\   <specVersion>\
\      <major>1</major>\
\      <minor>0</minor>\
\   </specVersion>\
\   <device>\
\      <deviceType>urn:schemas-upnp-org:device:MediaServer:1</deviceType>\
\      <hmx:X_HMXCAP>RCUTYPE_LEGACY_SPP</hmx:X_HMXCAP>\
\      <dlna:X_DLNACAP />\
\      <dlna:X_DLNADOC>DMS-1.50</dlna:X_DLNADOC>\
\      <friendlyName>HDR-7500T Media Server:192.168.1.1</friendlyName>\
\      <manufacturer>HUMAX</manufacturer>\
\      <manufacturerURL>www.humaxdigital.com</manufacturerURL>\
\      <modelDescription>Humax Media Server</modelDescription>\
\      <modelName>HDR-7500T</modelName>\
\      <modelNumber>Undefine</modelNumber>\
\      <modelURL>www.humaxdigital.com</modelURL>\
\      <serialNumber>HUMAX000378</serialNumber>\
\      <UDN>uuid:0E96FF1A-6771-2F44-A0C5-08EB740F494B</UDN>\
\      <serviceList>\
\         <service>\
\            <serviceType>urn:schemas-upnp-org:service:ConnectionManager:1</serviceType>\
\            <serviceId>urn:upnp-org:serviceId:ConnectionManager</serviceId>\
\            <SCPDURL>ConnectionManager/scpd.xml</SCPDURL>\
\            <controlURL>ConnectionManager/control</controlURL>\
\            <eventSubURL>ConnectionManager/event</eventSubURL>\
\         </service>\
\         <service>\
\            <serviceType>urn:schemas-upnp-org:service:ContentDirectory:1</serviceType>\
\            <serviceId>urn:upnp-org:serviceId:ContentDirectory</serviceId>\
\            <SCPDURL>ContentDirectory/scpd.xml</SCPDURL>\
\            <controlURL>ContentDirectory/control</controlURL>\
\            <eventSubURL>ContentDirectory/event</eventSubURL>\
\         </service>\
\      </serviceList>\
\   </device>\
\</root>"

testResponse :: C.ByteString
testResponse = C.pack "HTTP/1.1 200 OK\r\n\
\LOCATION: http://192.168.1.1:50001/\r\n\
\EXT:\r\n\
\SERVER: POSIX, UPnP/1.0\r\n\
\USN: uuid:0E96FF1A-6771-2F44-A0C5-08EB740F494B::urn:schemas-upnp-org:service:ConnectionManager:1\r\n\
\CACHE-CONTROL: max-age=600\r\n\
\ST: urn:schemas-upnp-org:service:ConnectionManager:1\r\n\
\\r\n"

testBadResponse :: C.ByteString
testBadResponse = C.pack "HTTP/1.1 200 OK\r\n\
\OCATION: http://192.168.1.1:50001/\r\n\
\EXT:\r\n\
\SERVER: POSIX, UPnP/1.0\r\n\
\USN: uuid:0E96FF1A-6771-2F44-A0C5-08EB740F494B::urn:schemas-upnp-org:service:ConnectionManager:1\r\n\
\CACHE-CONTROL: max-age=600\r\n\
\ST: urn:schemas-upnp-org:service:ConnectionManager:1\r\n\
\\r\n"

parseSuite :: Test
parseSuite = testGroup "Parse functions"
   [testCase "Parse UPNP Discovery" testParseDiscovery, testCase "Parse bad UPNP Discovery" testParseBadDiscovery,
    testCase "Parse Device XML" testParseDeviceServices]


expectedDevice = UPNPDevice { usn =
  T.pack "uuid:0E96FF1A-6771-2F44-A0C5-08EB740F494B::urn:schemas-upnp-org:service:ConnectionManager:1",
  location = T.pack "http://192.168.1.1:50001/"}

testParseDiscovery :: Assertion
testParseDiscovery = parseUPNPDiscovery testResponse @?= Right expectedDevice

testParseBadDiscovery :: Assertion
testParseBadDiscovery = parseUPNPDiscovery testBadResponse @?= Left "Missing location in response"

expectedDeviceServices :: Either XMLParseError [DeviceService]
expectedDeviceServices = Right [DeviceService {serviceType="urn:schemas-upnp-org:service:ConnectionManager:1",
            serviceId="urn:upnp-org:serviceId:ConnectionManager",
            scpdURL="ConnectionManager/scpd.xml",
            controlURL="ConnectionManager/control",
            eventSubURL="ConnectionManager/event"}, DeviceService {serviceType="urn:schemas-upnp-org:service:ContentDirectory:1",
            serviceId="urn:upnp-org:serviceId:ContentDirectory",
            scpdURL="ContentDirectory/scpd.xml",
            controlURL="ContentDirectory/control",
            eventSubURL="ContentDirectory/event"}]

testParseDeviceServices :: Assertion
testParseDeviceServices = parseDeviceServices (fromDocument $ parseText_ def deviceXML) @?= expectedDeviceServices

testSampleParse :: Assertion
testSampleParse =
  let cursor = fromDocument $ parseText_ def deviceXML
      servicesNode = cursor $// laxElement "device" &/ laxElement "serviceList" &// laxElement "service"
  in
    "Hello" @?= show servicesNode
