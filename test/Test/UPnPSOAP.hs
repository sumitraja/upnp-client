{-# LANGUAGE OverloadedStrings #-}

module Test.UPnPSOAP where

import ClassyPrelude
import Test.Framework (testGroup, Test)
import Test.Framework.Providers.HUnit
import Test.HUnit hiding (Test)
import Network.UPnP.Lib.UPnPSOAP
import Text.XML
import Text.XML.Cursor
import Text.Hamlet.XML
import qualified Data.ByteString.Char8 as C
import qualified Data.Text as T
import qualified Data.Map.Lazy as Map
import Network.UPnP.Lib.UPnPCommands
import Network.UPnP.Lib.UPnPResponse
import Network.UPnP.Lib.XMLStuff

upnpXMLSuite :: Test
upnpXMLSuite = testGroup "UPNP XML functions"
   [testCase "Create UPNP Envelope" testCreateEnvelope,
    testCase "Parse UPNPError XML" testParseUPNPError,
    testCase "Parse SOAP Error XML" testParseSOAPError,
    testCase "Parse SOAP Envelope" testParseEnvelope]

browseXML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\
\<soap:Envelope soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" \
\xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\
\<soap:Body>\
\<u:Browse xmlns:u=\"urn:schemas-upnp-org:service:ContentDirectory:1\"><ObjectID>0/1/2</ObjectID>\
\<BrowseFlag>BrowseDirectChildren</BrowseFlag>\
\<Filter>*</Filter>\
\<StartingIndex>0</StartingIndex>\
\<RequestedCount>10</RequestedCount>\
\<SortCriteria><![CDATA[]]></SortCriteria>\
\</u:Browse>\
\</soap:Body></soap:Envelope>"

soapEnv = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\
\<soap:Envelope soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" \
\xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\
\<soap:Body>\
\<u:Browse xmlns:u=\"urn:schemas-upnp-org:service:ContentDirectory:1\"><ObjectID>0/1/2</ObjectID>\
\</u:Browse>\
\</soap:Body></soap:Envelope>"

testParseEnvelope :: Assertion
testParseEnvelope = parseSOAPEnvelope doco nullParser nullParser @?= Right (SOAPEnvelope ())
  where
    doco = fromDocument (parseText_ def soapEnv)
    nullParser :: XMLParser ()
    nullParser cur = Right ()

testCreateEnvelope :: Assertion
testCreateEnvelope = soapEnvelope (Browse "0/1/2" BrowseDirectChildren All 0 10 []) @?= documentRoot (parseText_ def browseXML)

upnpErrorXML = "<detail><UPnPError xmlns=\"urn:schemas-upnp-org:control-1-0\"><errorCode>501</errorCode><errorDescription>Invalid XML</errorDescription></UPnPError></detail>"
upnpError = UPNPError 501 "Invalid XML"

testParseUPNPError :: Assertion
testParseUPNPError = parseUPNPError (fromDocument (parseText_ def upnpErrorXML)) @?= Right upnpError

soapBodyErrorXML = "<s:Fault xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><faultcode>s:Client</faultcode><faultstring>UPnPError</faultstring>\
\ <detail><UPnPError xmlns=\"urn:schemas-upnp-org:control-1-0\"><errorCode>501</errorCode><errorDescription>Invalid XML</errorDescription></UPnPError></detail></s:Fault>"
soapError = SOAPFault "s:Client" "UPnPError" upnpError

testParseSOAPError :: Assertion
testParseSOAPError = parseSOAPError (fromDocument (parseText_ def soapBodyErrorXML)) parseUPNPError @?= Right soapError
