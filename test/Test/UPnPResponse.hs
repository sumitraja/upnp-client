{-# LANGUAGE OverloadedStrings, DisambiguateRecordFields #-}

module Test.UPnPResponse where

import Test.Framework (testGroup, Test)
import Test.Framework.Providers.HUnit
import Test.HUnit hiding (Test)
import Text.XML
import Text.XML.Cursor
import Text.Hamlet.XML
import qualified Data.ByteString.Char8 as C
import qualified Data.Text as T
import qualified Data.Map.Lazy as Map
import Network.UPnP.Lib.XMLStuff
import Network.UPnP.Lib.DIDL
import Network.UPnP.Lib.UPnPSOAP
import Network.UPnP.Lib.UPnPResponse
import ClassyPrelude

upnpResponseSuite :: Test
upnpResponseSuite = testGroup "UPnPResponse functions"
   [testCase "Parse UPnP successful Browse" testParseUPnPBrowseSuccess]

upnpSuccess = "<u:BrowseResponse xmlns:u=\"urn:schemas-upnp-org:service:ContentDirectory:1\"><Result>&lt;DIDL-Lite xmlns:dc=&quot;http://purl.org/dc/elements/1.1/&quot; xmlns:upnp=&quot;urn:schemas-upnp-org:metadata-1-0/upnp/&quot; xmlns=&quot;urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/&quot; xmlns:dlna=&quot;urn:schemas-dlna-org:metadata-1-0/&quot;&gt;\
\ &lt;container id=&quot;0\1\2&quot; parentID=&quot;0\1&quot; restricted=&quot;0&quot; searchable=&quot;1&quot; childCount=&quot;61&quot;&gt;\
\ &lt;dc:title&gt;My Video&lt;/dc:title&gt;\
\ &lt;upnp:class&gt;object.container&lt;/upnp:class&gt;\
\ &lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.container&lt;/upnp:createClass&gt;\
\ &lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.imageItem&lt;/upnp:createClass&gt;\
\ &lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.audioItem&lt;/upnp:createClass&gt;\
\ &lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.videoItem&lt;/upnp:createClass&gt;\
\ &lt;dc:creator&gt;HUMAX&lt;/dc:creator&gt;\
\ &lt;/container&gt;\
\ &lt;/DIDL-Lite&gt;</Result>\
\ <NumberReturned>1</NumberReturned>\
\ <TotalMatches>1</TotalMatches>\
\ <UpdateID>0</UpdateID>\
\ </u:BrowseResponse>"

soapSuccess = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\
\ <s:Envelope s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">\
\ <s:Body>\
\ <u:BrowseResponse xmlns:u=\"urn:schemas-upnp-org:service:ContentDirectory:1\"><Result>&lt;DIDL-Lite xmlns:dc=&quot;http://purl.org/dc/elements/1.1/&quot; xmlns:upnp=&quot;urn:schemas-upnp-org:metadata-1-0/upnp/&quot; xmlns=&quot;urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/&quot; xmlns:dlna=&quot;urn:schemas-dlna-org:metadata-1-0/&quot;&gt;\
\ &lt;container id=&quot;0\1\2&quot; parentID=&quot;0\1&quot; restricted=&quot;0&quot; searchable=&quot;1&quot; childCount=&quot;61&quot;&gt;\
\ &lt;dc:title&gt;My Video&lt;/dc:title&gt;\
\ &lt;upnp:class&gt;object.container&lt;/upnp:class&gt;\
\ &lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.container&lt;/upnp:createClass&gt;\
\ &lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.imageItem&lt;/upnp:createClass&gt;\
\ &lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.audioItem&lt;/upnp:createClass&gt;\
\ &lt;upnp:createClass includeDerived=&quot;1&quot;&gt;object.item.videoItem&lt;/upnp:createClass&gt;\
\ &lt;dc:creator&gt;HUMAX&lt;/dc:creator&gt;\
\ &lt;/container&gt;\
\ &lt;/DIDL-Lite&gt;</Result>\
\ <NumberReturned>1</NumberReturned>\
\ <TotalMatches>1</TotalMatches>\
\ <UpdateID>0</UpdateID>\
\ </u:BrowseResponse>\
\   </s:Body>\
\ </s:Envelope>"

testParseUPnPBrowseSuccess :: Assertion
testParseUPnPBrowseSuccess = do
  let env = parseSOAPEnvelope doco parseUPNPError (parseUPNPBrowseResponse parseDIDL)
  case env of
    Left err -> assertFailure (unpack err)
    Right soapenv -> checkSOAPEnvelope soapenv
  where
    doco = fromDocument (parseText_ def soapSuccess)

checkSOAPEnvelope :: SOAPEnvelope UPNPError (UPNPBrowseResponse DIDLResponse) -> Assertion
checkSOAPEnvelope (SOAPFaultEnvelope sfault) = assertFailure "SOAPFault not expected"
checkSOAPEnvelope (SOAPEnvelope UPNPBrowseResponse {result = didl}) = return ()
