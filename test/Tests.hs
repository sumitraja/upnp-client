module Main where

import Test.Framework (defaultMain)
import Test.DeviceDiscovery
import Test.UPnPSOAP
import Test.UPnPCommands
import Test.DIDL
import Test.XMLStuff
import Test.UPnPResponse
import ClassyPrelude

main :: IO ()
main = defaultMain [parseSuite, upnpXMLSuite, upnpCommandSuite, didlSuite, xmlSuite, upnpResponseSuite]
