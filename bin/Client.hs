
import Network.UPnP.Client.UPnPClient
import Options.Applicative
import ClassyPrelude

main :: IO ()
main = execParser opts >>= optionHandler where
    opts = parseOptions `withInfo` "UPNP Content Browser"
